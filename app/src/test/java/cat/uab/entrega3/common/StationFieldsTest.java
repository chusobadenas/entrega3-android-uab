package cat.uab.entrega3.common;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StationFieldsTest {

  @Test
  public void testConstantValues() {
    assertEquals(StationFields.CLOUDS, "clouds");
    assertEquals(StationFields.COUNTRY_CODE, "countryCode");
    assertEquals(StationFields.DATE_TIME, "datetime");
    assertEquals(StationFields.ELEVATION, "elevation");
    assertEquals(StationFields.HUMIDITY, "humidity");
    assertEquals(StationFields.ICAO, "ICAO");
    assertEquals(StationFields.LATITUDE, "lat");
    assertEquals(StationFields.LENGTH, "lng");
    assertEquals(StationFields.NAME, "stationName");
    assertEquals(StationFields.TEMPERATURE, "temperature");
    assertEquals(StationFields.WEATHER_CONDITION, "weatherCondition");
    assertEquals(StationFields.WIND_DIRECTION, "windDirection");
    assertEquals(StationFields.WIND_SPEED, "windSpeed");
    assertEquals(StationFields.OPTIONS, "stationOptions");
    assertEquals(StationFields.USERNAME, "chusobadenas");
  }
}
