package cat.uab.entrega3.exception;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CoordinateExceptionTest {

  private static final String EXCEPTION_MSG = "This is an exception";

  @Test
  public void testCreationSuccess() {
    CoordinateException exception = new CoordinateException(EXCEPTION_MSG);

    assertNotNull("Exception created", exception);
    assertEquals("Exception message", exception.getMessage(), EXCEPTION_MSG);
  }
}