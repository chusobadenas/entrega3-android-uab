package cat.uab.entrega3.request;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StationListRequestGeneratorTest {

  @Test
  public void testUrlGenerationSuccess() {
    StationsListRequestGenerator generator = new StationsListRequestGenerator("10", "22", "33",
        "88");
    String requestUrl = generator.generateRequestUrl();

    assertEquals("URL", requestUrl, "http://api.geonames.org/weatherJSON?" +
        "north=10&south=22&east=33&west=88&username=chusobadenas");
  }
}
