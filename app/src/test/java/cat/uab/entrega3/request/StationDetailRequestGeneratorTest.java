package cat.uab.entrega3.request;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StationDetailRequestGeneratorTest {

  @Test
  public void testUrlGenerationSuccess() {
    StationDetailRequestGenerator generator = new StationDetailRequestGenerator("UPPA");
    String requestUrl = generator.generateRequestUrl();

    assertEquals("URL", requestUrl, "http://api.geonames.org/weatherIcaoJSON?" +
        "ICAO=UPPA&username=chusobadenas");
  }
}
