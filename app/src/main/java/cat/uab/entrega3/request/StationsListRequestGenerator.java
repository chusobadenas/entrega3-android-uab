package cat.uab.entrega3.request;

import cat.uab.entrega3.common.StationFields;

/**
 * Stations list HTTP request generator
 */
public class StationsListRequestGenerator {

  private static final String HOST_URL = "http://api.geonames.org/weatherJSON?";

  private final String north;
  private final String south;
  private final String east;
  private final String west;

  /**
   * Constructor
   *
   * @param north north orientation in degrees
   * @param south south orientation in degrees
   * @param east  east orientation in degrees
   * @param west  west orientation in degrees
   */
  public StationsListRequestGenerator(String north, String south, String east, String west) {
    this.north = north;
    this.south = south;
    this.east = east;
    this.west = west;
  }

  /**
   * @return the url to execute
   */
  public final String generateRequestUrl() {
    return HOST_URL + "north=" + north + "&south=" + south + "&east=" + east + "&west=" + west +
        "&username=" + StationFields.USERNAME;
  }
}
