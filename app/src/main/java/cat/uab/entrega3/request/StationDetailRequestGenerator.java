package cat.uab.entrega3.request;

import cat.uab.entrega3.common.StationFields;

/**
 * Station detail HTTP request generator
 */
public class StationDetailRequestGenerator {

  private static final String HOST_URL = "http://api.geonames.org/weatherIcaoJSON?";

  private final String icao;

  /**
   * Constructor
   *
   * @param icao station ICAO
   */
  public StationDetailRequestGenerator(String icao) {
    this.icao = icao;
  }

  /**
   * @return the url to execute
   */
  public final String generateRequestUrl() {
    return HOST_URL + "ICAO=" + icao + "&username=" + StationFields.USERNAME;
  }
}
