package cat.uab.entrega3.exception;

/**
 * This exception is thrown when a coordinate is empty
 */
public class CoordinateException extends Exception {

  /**
   * Constructor
   *
   * @param detailMessage exception message
   */
  public CoordinateException(String detailMessage) {
    super(detailMessage);
  }
}
