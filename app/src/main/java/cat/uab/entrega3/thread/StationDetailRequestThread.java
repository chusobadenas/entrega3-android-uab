package cat.uab.entrega3.thread;

import android.content.ContentValues;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

import cat.uab.entrega3.common.StationFields;

/**
 * Executes HTTP GET request to obtain station detail
 */
public class StationDetailRequestThread extends AbstractRequestThread implements Runnable {

  public static final int SHOW_DETAILS_MSG = 0;

  private static final String HYPHEN = "-";

  /**
   * Constructor
   *
   * @param requestUrl request URL
   * @param handler    handler
   */
  public StationDetailRequestThread(URL requestUrl, Handler handler) {
    super(requestUrl, handler);
  }

  ContentValues processResult(JSONObject result) {
    ContentValues stationDetails = new ContentValues();

    // Only if the task executes successfully
    if (success) {
      try {
        JSONObject station = result.getJSONObject("weatherObservation");

        stationDetails.put(StationFields.CLOUDS, getJSONValue(station, StationFields.CLOUDS));
        stationDetails.put(StationFields.COUNTRY_CODE, getJSONValue(station, StationFields.COUNTRY_CODE));
        stationDetails.put(StationFields.DATE_TIME, getJSONValue(station, StationFields.DATE_TIME));
        stationDetails.put(StationFields.ELEVATION, getJSONValue(station, StationFields.ELEVATION));
        stationDetails.put(StationFields.HUMIDITY, getJSONValue(station, StationFields.HUMIDITY));
        stationDetails.put(StationFields.ICAO, getJSONValue(station, StationFields.ICAO));
        stationDetails.put(StationFields.LATITUDE, getJSONValue(station, StationFields.LATITUDE));
        stationDetails.put(StationFields.LENGTH, getJSONValue(station, StationFields.LENGTH));
        stationDetails.put(StationFields.TEMPERATURE, getJSONValue(station, StationFields.TEMPERATURE));
        stationDetails.put(StationFields.WEATHER_CONDITION, getJSONValue(station, StationFields.WEATHER_CONDITION));
        stationDetails.put(StationFields.WIND_DIRECTION, getJSONValue(station, StationFields.WIND_DIRECTION));
        stationDetails.put(StationFields.WIND_SPEED, getJSONValue(station, StationFields.WIND_SPEED));

      } catch (JSONException exception) {
        Log.e(null, "Fail to parse JSON", exception);
      }
    }

    return stationDetails;
  }

  private String getJSONValue(JSONObject jsonObject, String key) {
    String value = HYPHEN;

    try {
      value = jsonObject.getString(key).isEmpty() ? HYPHEN : jsonObject.getString(key);
    } catch (JSONException exception) {
      Log.e(null, "Fail to parse JSON", exception);
    }

    return value;
  }

  @Override
  public void run() {
    // Download data
    JSONObject data = doGet();

    // Process data
    ContentValues stationDetails = processResult(data);

    // Show details
    Message msg = handler.obtainMessage(SHOW_DETAILS_MSG, stationDetails);
    handler.sendMessage(msg);
  }
}
