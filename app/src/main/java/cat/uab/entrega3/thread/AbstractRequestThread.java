package cat.uab.entrega3.thread;

import android.content.ContentValues;
import android.os.Handler;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Common methods for request threads
 */
abstract class AbstractRequestThread {

  private final URL requestUrl;
  final Handler handler;

  boolean success = false;

  /**
   * Constructor
   *
   * @param requestUrl request URL
   * @param handler    handler
   */
  AbstractRequestThread(URL requestUrl, Handler handler) {
    this.requestUrl = requestUrl;
    this.handler = handler;
  }

  JSONObject doGet() {
    JSONObject json = null;
    HttpURLConnection connection = null;

    try {
      // Execute connection
      connection = (HttpURLConnection) requestUrl.openConnection();
      BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

      // Read result
      String line;
      StringBuilder result = new StringBuilder();
      while ((line = reader.readLine()) != null) {
        result.append(line);
      }

      // Close stream
      reader.close();

      // Parse JSON
      json = new JSONObject(result.toString());
      success = true;

    } catch (IOException exception) {
      Log.e(null, "Connection failed", exception);
    } catch (JSONException exception) {
      Log.e(null, "Fail to parse JSON", exception);

    } finally {
      // Close connection
      if (connection != null) {
        connection.disconnect();
      }
    }

    return json;
  }

  /**
   * Process result from HTTP GET
   *
   * @param result JSON result from HTTP GET
   * @return processed data
   */
  abstract ContentValues processResult(JSONObject result);
}
