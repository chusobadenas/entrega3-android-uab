package cat.uab.entrega3.thread;

import android.content.ContentValues;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.Map;
import java.util.TreeMap;

import cat.uab.entrega3.StationListHandler;
import cat.uab.entrega3.common.StationFields;

/**
 * Executes HTTP GET request to obtain stations list
 */
public class StationListRequestThread extends AbstractRequestThread implements Runnable {

  private Map.Entry<Float, ContentValues> minTemp;

  /**
   * Constructor
   *
   * @param requestUrl request URL
   * @param handler    handler
   */
  public StationListRequestThread(URL requestUrl, Handler handler) {
    super(requestUrl, handler);
  }

  ContentValues processResult(JSONObject jsonResult) {
    ContentValues stationsList = new ContentValues();

    // Temperatures
    // key -> temperature
    // value -> ICAO and NAME of the station
    Map<Float, ContentValues> temperatures = new TreeMap<>();

    // Only if the task executes successfully
    if (success) {
      try {
        JSONArray stationsArray = jsonResult.getJSONArray("weatherObservations");

        for (int i = 0; i < stationsArray.length(); i++) {
          JSONObject station = stationsArray.getJSONObject(i);

          // Only need ICAO and Name
          String stationIcao = station.getString(StationFields.ICAO);
          String stationName = station.getString(StationFields.NAME);
          stationsList.put(stationIcao, stationName);

          // Add temperature
          ContentValues icaoAndName = new ContentValues();
          icaoAndName.put(stationIcao, stationName);
          addTemperature(temperatures, station, icaoAndName);
        }
      } catch (JSONException exception) {
        Log.e(null, "Fail to parse JSON", exception);
      }
    }

    // Calculate min temperature
    minTemp = temperatures.isEmpty() ? null : temperatures.entrySet().iterator().next();

    return stationsList;
  }

  private void addTemperature(Map<Float, ContentValues> temperatures, JSONObject station,
                              ContentValues icaoAndName) {
    try {
      // Read temperature
      String currentTempStr = station.getString(StationFields.TEMPERATURE);

      // Check value
      if (!currentTempStr.isEmpty()) {
        temperatures.put(Float.valueOf(currentTempStr), icaoAndName);
      }
    } catch (JSONException exception) {
      Log.d(null, "Temperature is not defined", exception);
    }
  }

  @Override
  public void run() {
    // Show progress bar
    Message msg = handler.obtainMessage(StationListHandler.PROGRESS_BAR_VISIBILITY, View.VISIBLE);
    handler.sendMessage(msg);

    // Hide stations list
    msg = handler.obtainMessage(StationListHandler.STATIONS_LIST_VISIBILITY, View.GONE);
    handler.sendMessage(msg);

    // Download data
    JSONObject data = doGet();

    // Process data
    ContentValues stationsList = processResult(data);

    // Load stations list
    msg = handler.obtainMessage(StationListHandler.SHOW_STATIONS_LIST, stationsList);
    handler.sendMessage(msg);

    // Hide progress bar
    msg = handler.obtainMessage(StationListHandler.PROGRESS_BAR_VISIBILITY, View.GONE);
    handler.sendMessage(msg);

    // Show stations list
    msg = handler.obtainMessage(StationListHandler.STATIONS_LIST_VISIBILITY, View.VISIBLE);
    handler.sendMessage(msg);

    // Send notification
    msg = handler.obtainMessage(StationListHandler.SEND_NOTIFICATION, minTemp);
    handler.sendMessage(msg);
  }
}
