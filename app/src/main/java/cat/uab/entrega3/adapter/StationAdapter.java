package cat.uab.entrega3.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cat.uab.entrega3.R;

/**
 * Adapter for a weather station
 */
public class StationAdapter extends BaseAdapter {

  private final Context context;
  private final List<Map.Entry<String, String>> list;

  /**
   * Constructor
   *
   * @param context activity context
   * @param list    list of stations
   */
  public StationAdapter(Context context, List<Map.Entry<String, String>> list) {
    super();
    this.context = context;
    this.list = new ArrayList<>(list);
  }

  @Override
  public int getCount() {
    return list.size();
  }

  @Override
  public Object getItem(int position) {
    Map.Entry<String, String> entry = list.get(position);
    return entry == null ? null : entry.getValue();
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    // Reuse the view of each row. Check always for null to create a new
    // view, otherwise reuse and redraw it!
    View elementView = convertView;

    if (elementView == null) {
      LayoutInflater inflater = LayoutInflater.from(context);
      elementView = inflater.inflate(R.layout.station_list_item, parent, false);
    }

    // Read station
    Map.Entry<String, String> station = list.get(position);

    if (station != null) {
      // Get views
      TextView stationKey = (TextView) elementView.findViewById(R.id.itemKey);
      TextView stationName = (TextView) elementView.findViewById(R.id.itemName);

      // Update views
      stationKey.setText(station.getKey());
      stationName.setText(station.getValue());
    }

    return elementView;
  }
}
