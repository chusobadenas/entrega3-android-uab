package cat.uab.entrega3;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import cat.uab.entrega3.common.StationFields;
import cat.uab.entrega3.exception.CoordinateException;
import cat.uab.entrega3.preferences.PreferencesActivity;
import cat.uab.entrega3.request.StationsListRequestGenerator;
import cat.uab.entrega3.thread.StationListRequestThread;

/**
 * Shows the list of weather stations
 */
public class StationsListActivity extends AppCompatActivity {

  private static final int PREF_REQ_CODE = 0;

  private ProgressBar progressBar;
  private ListView stationsList;

  private TextView northView;
  private TextView southView;
  private TextView eastView;
  private TextView westView;

  private ContentValues preferencesMap = new ContentValues();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_stations_list);

    // Get views
    progressBar = (ProgressBar) findViewById(R.id.progressBar);
    stationsList = (ListView) findViewById(R.id.stationsList);
    northView = (TextView) findViewById(R.id.north);
    southView = (TextView) findViewById(R.id.south);
    eastView = (TextView) findViewById(R.id.east);
    westView = (TextView) findViewById(R.id.west);

    Button searchButton = (Button) findViewById(R.id.searchButton);

    // Change title
    getSupportActionBar().setTitle(R.string.stations_title);

    // Default preferences
    PreferenceManager.setDefaultValues(this, R.xml.user_preferences, false);
    updatePreferences();

    // Load default stations
    loadStations();

    // Search for stations
    searchButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        loadStations();
      }
    });

    // Click on station
    stationsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        showStationDetails(view);
      }
    });
  }

  private void loadStations() {
    String north = northView.getText().toString();
    String south = southView.getText().toString();
    String east = eastView.getText().toString();
    String west = westView.getText().toString();

    try {
      // Validate coordinates
      validateInput(northView, north);
      validateInput(southView, south);
      validateInput(eastView, east);
      validateInput(westView, west);

      // Generate request URL
      StationsListRequestGenerator requestGenerator = new StationsListRequestGenerator(north,
          south, east, west);
      String requestUrl = requestGenerator.generateRequestUrl();
      URL url = new URL(requestUrl);

      // Invoke REST service
      Handler handler = new Handler(new StationListHandler(this, progressBar, stationsList, preferencesMap));
      new Thread(new StationListRequestThread(url, handler)).start();

    } catch (CoordinateException exception) {
      Log.e(null, "Coordinate in wrong format", exception);
    } catch (MalformedURLException exception) {
      Log.e(null, "URL in wrong format", exception);
    }
  }

  private void validateInput(TextView view, String coordinate) throws CoordinateException {
    // It is empty?
    if (coordinate == null || coordinate.isEmpty()) {
      String errorMsg = getResources().getString(R.string.validate_error);
      view.setError(errorMsg);
      throw new CoordinateException(errorMsg);
    }
  }

  private void showStationDetails(View view) {
    // Get station values
    TextView icaoView = (TextView) view.findViewById(R.id.itemKey);
    TextView nameView = (TextView) view.findViewById(R.id.itemName);

    // Launch activity detail
    Intent intent = new Intent(StationsListActivity.this, StationDetailActivity.class);
    intent.putExtra(StationFields.ICAO, icaoView.getText().toString());
    intent.putExtra(StationFields.NAME, nameView.getText().toString());
    intent.putExtra(StationFields.OPTIONS, preferencesMap);
    startActivity(intent);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    boolean result = super.onOptionsItemSelected(item);

    // Preferences item
    if (item.getItemId() == R.id.preferences) {
      result = true;
      showPreferences();
    }

    return result;
  }

  private void showPreferences() {
    Intent intent = new Intent(this, PreferencesActivity.class);
    startActivityForResult(intent, PREF_REQ_CODE);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    // Preferences code
    if (requestCode == PREF_REQ_CODE) {
      updatePreferences();
    }
  }

  private void updatePreferences() {
    // Reset values
    preferencesMap = new ContentValues();

    // Get preferences
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

    // Update station detail view from preferences
    Set<String> detailsToShow = preferences.getStringSet("station_details_options", new
        HashSet<String>());

    for (String stationDetail : detailsToShow) {
      preferencesMap.put(stationDetail, true);
    }
  }
}
