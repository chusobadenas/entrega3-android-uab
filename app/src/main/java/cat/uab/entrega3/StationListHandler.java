package cat.uab.entrega3;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.NotificationCompat;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cat.uab.entrega3.adapter.StationAdapter;

/**
 * Handler to manage stations list
 */
public class StationListHandler implements Handler.Callback {

  public static final int PROGRESS_BAR_VISIBILITY = 0;
  public static final int STATIONS_LIST_VISIBILITY = 1;
  public static final int SHOW_STATIONS_LIST = 2;
  public static final int SEND_NOTIFICATION = 3;

  private static final int NOTIFICATION_ID = 0;

  private final Context context;
  private final ProgressBar progressBar;
  private final ListView stationsList;
  private final ContentValues preferencesMap;

  /**
   * Constructor
   *
   * @param context        activity context
   * @param progressBar    progress bar
   * @param stationsList   list of weather stations
   * @param preferencesMap preferences
   */
  public StationListHandler(Context context, ProgressBar progressBar, ListView stationsList,
                            ContentValues preferencesMap) {
    this.context = context;
    this.progressBar = progressBar;
    this.stationsList = stationsList;
    this.preferencesMap = preferencesMap;
  }

  @Override
  public boolean handleMessage(Message msg) {
    switch (msg.what) {
      case PROGRESS_BAR_VISIBILITY:
        progressBar.setVisibility((int) msg.obj);
        break;

      case STATIONS_LIST_VISIBILITY:
        stationsList.setVisibility((int) msg.obj);
        break;

      case SHOW_STATIONS_LIST:
        // Conversion
        ContentValues values = (ContentValues) msg.obj;
        List<Map.Entry<String, String>> list = new ArrayList(values.valueSet());

        // Load stations
        StationAdapter adapter = new StationAdapter(context, list);
        stationsList.setAdapter(adapter);
        break;

      case SEND_NOTIFICATION:
        Map.Entry<Float, ContentValues> minTemp = (Map.Entry<Float, ContentValues>) msg.obj;

        if (minTemp != null) {
          sendNotification(minTemp);
        }
        break;

      default:
        break;
    }

    return true;
  }

  private void sendNotification(Map.Entry<Float, ContentValues> minTemp) {
    // Manager
    NotificationManager notificationManager = (NotificationManager) context.getSystemService(
        Context.NOTIFICATION_SERVICE);

    // Prepare
    Float temp = minTemp.getKey();
    ContentValues icaoAndName = minTemp.getValue();
    String icao = icaoAndName.keySet().iterator().next();
    String name = icaoAndName.getAsString(icao);

    NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
    builder.setAutoCancel(true)
        .setSmallIcon(R.drawable.snow)
        .setWhen(System.currentTimeMillis())
        .setContentTitle(context.getResources().getString(R.string.notification_title) + " " + temp)
        .setContentText(name)
        .setContentIntent(StationDetailActivity.getCallingPendingIntent(context, icao, name,
            preferencesMap));

    // Create and send
    Notification notification = builder.build();
    notificationManager.notify(NOTIFICATION_ID, notification);
  }
}
