package cat.uab.entrega3;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.net.MalformedURLException;
import java.net.URL;

import cat.uab.entrega3.common.StationFields;
import cat.uab.entrega3.request.StationDetailRequestGenerator;
import cat.uab.entrega3.thread.StationDetailRequestThread;

/**
 * Shows the detail of a weather station
 */
public class StationDetailActivity extends AppCompatActivity {

  private static final int ACT_REQ_CODE = 0;

  private final Handler handler = new Handler(new Handler.Callback() {
    @Override
    public boolean handleMessage(Message msg) {

      if (msg.what == StationDetailRequestThread.SHOW_DETAILS_MSG) {
        // Show station details
        ContentValues stationDetails = (ContentValues) msg.obj;
        showDetails(stationDetails);
      }

      return true;
    }
  });

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.station_detail);

    // Change title
    getSupportActionBar().setTitle(R.string.station_detail_title);

    // Load station details
    loadDetails();
  }

  private void loadDetails() {
    try {
      // Generate request URL
      String icao = getIntent().getStringExtra(StationFields.ICAO);
      StationDetailRequestGenerator requestGenerator = new StationDetailRequestGenerator(icao);

      String requestUrl = requestGenerator.generateRequestUrl();
      URL url = new URL(requestUrl);

      // Invoke REST service
      new Thread(new StationDetailRequestThread(url, handler)).start();

    } catch (MalformedURLException exception) {
      Log.e(null, "URL in wrong format", exception);
    }
  }

  private void showDetails(ContentValues values) {
    // Read preferences
    ContentValues preferencesMap = getIntent().getParcelableExtra(StationFields.OPTIONS);

    // Get views
    TextView nameView = (TextView) findViewById(R.id.stationName);
    TextView cloudsView = (TextView) findViewById(R.id.stationClouds);
    TextView countryCodeView = (TextView) findViewById(R.id.stationCountryCode);
    TextView dateTimeView = (TextView) findViewById(R.id.stationDateTime);
    TextView elevationView = (TextView) findViewById(R.id.stationElevation);
    TextView humidityView = (TextView) findViewById(R.id.stationHumidity);
    TextView icaoView = (TextView) findViewById(R.id.stationIcao);
    TextView latitudeView = (TextView) findViewById(R.id.stationLatitude);
    TextView lengthView = (TextView) findViewById(R.id.stationLength);
    TextView temperatureView = (TextView) findViewById(R.id.stationTemperature);
    TextView weatherConditionView = (TextView) findViewById(R.id.stationWeatherCondition);
    TextView windDirectionView = (TextView) findViewById(R.id.stationWindDirection);
    TextView windSpeedView = (TextView) findViewById(R.id.stationWindSpeed);

    // Update views
    displayValue(nameView, getIntent().getStringExtra(StationFields.NAME), true);
    displayValue(cloudsView, values.getAsString(StationFields.CLOUDS), elementMustBeShown
        (preferencesMap, StationFields.CLOUDS));
    displayValue(countryCodeView, values.getAsString(StationFields.COUNTRY_CODE), elementMustBeShown
        (preferencesMap, StationFields.COUNTRY_CODE));
    displayValue(dateTimeView, values.getAsString(StationFields.DATE_TIME), elementMustBeShown
        (preferencesMap, StationFields.DATE_TIME));
    displayValue(elevationView, values.getAsString(StationFields.ELEVATION), elementMustBeShown
        (preferencesMap, StationFields.ELEVATION));
    displayValue(humidityView, values.getAsString(StationFields.HUMIDITY), elementMustBeShown
        (preferencesMap, StationFields.HUMIDITY));
    displayValue(icaoView, values.getAsString(StationFields.ICAO), elementMustBeShown
        (preferencesMap, StationFields.ICAO));
    displayValue(latitudeView, values.getAsString(StationFields.LATITUDE), elementMustBeShown
        (preferencesMap, StationFields.LATITUDE));
    displayValue(lengthView, values.getAsString(StationFields.LENGTH), elementMustBeShown
        (preferencesMap, StationFields.LENGTH));
    displayValue(temperatureView, values.getAsString(StationFields.TEMPERATURE), elementMustBeShown
        (preferencesMap, StationFields.TEMPERATURE));
    displayValue(weatherConditionView, values.getAsString(StationFields.WEATHER_CONDITION),
        elementMustBeShown(preferencesMap, StationFields.WEATHER_CONDITION));
    displayValue(windDirectionView, values.getAsString(StationFields.WIND_DIRECTION),
        elementMustBeShown(preferencesMap, StationFields.WIND_DIRECTION));
    displayValue(windSpeedView, values.getAsString(StationFields.WIND_SPEED), elementMustBeShown
        (preferencesMap, StationFields.WIND_SPEED));
  }

  private boolean elementMustBeShown(ContentValues map, String key) {
    boolean result = false;

    if (map.containsKey(key)) {
      result = true;
    }

    return result;
  }

  private void displayValue(TextView view, String value, boolean isShow) {
    // Show value
    if (isShow) {
      view.setText(value);
    }
    // Hide value
    else {
      view.setText(R.string.hidden_value);
    }
  }

  /**
   * Creates a pending intent for this activity itself
   *
   * @param context        activity context
   * @param icao           station ICAO
   * @param name           station name
   * @param preferencesMap preferences
   * @return a pending intent of this activity
   */
  public static PendingIntent getCallingPendingIntent(Context context, String icao, String name,
                                                      ContentValues preferencesMap) {
    Intent intent = new Intent(context, StationDetailActivity.class);
    intent.putExtra(StationFields.ICAO, icao);
    intent.putExtra(StationFields.NAME, name);
    intent.putExtra(StationFields.OPTIONS, preferencesMap);
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

    return PendingIntent.getActivity(context, ACT_REQ_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
  }
}
