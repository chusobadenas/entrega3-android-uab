package cat.uab.entrega3.common;

/**
 * Station fields constant values
 */
public final class StationFields {

  private StationFields() {
    // Nothing to do
  }

  public static final String CLOUDS = "clouds";

  public static final String COUNTRY_CODE = "countryCode";

  public static final String DATE_TIME = "datetime";

  public static final String ELEVATION = "elevation";

  public static final String HUMIDITY = "humidity";

  public static final String ICAO = "ICAO";

  public static final String LATITUDE = "lat";

  public static final String LENGTH = "lng";

  public static final String NAME = "stationName";

  public static final String TEMPERATURE = "temperature";

  public static final String WEATHER_CONDITION = "weatherCondition";

  public static final String WIND_DIRECTION = "windDirection";

  public static final String WIND_SPEED = "windSpeed";

  public static final String OPTIONS = "stationOptions";

  public static final String USERNAME = "chusobadenas";
}
