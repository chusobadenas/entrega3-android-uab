package cat.uab.entrega3.preferences;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;

import cat.uab.entrega3.R;

/**
 * User preferences activity
 */
public class PreferencesActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Change title
    getSupportActionBar().setTitle(R.string.preferences);

    // Load fragment
    PreferenceFragment fragment = new PreferencesFragment();

    FragmentTransaction transaction = getFragmentManager().beginTransaction();
    transaction.replace(android.R.id.content, fragment);
    transaction.commit();
  }
}