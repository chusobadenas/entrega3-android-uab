package cat.uab.entrega3.preferences;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import cat.uab.entrega3.R;

/**
 * User preferences fragment
 */
public class PreferencesFragment extends PreferenceFragment {

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Preferences screen
    addPreferencesFromResource(R.xml.user_preferences);
  }
}